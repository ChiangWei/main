DesignPatterns
=====
* ### Introduction
* ### Python
* ### Java
<br />

Design Pattern 應用紀錄
=====
* ### 整合網管
    * ### Singleton (組態管理、資訊管理、資訊同步、遠端控制、限速管理)
    * ### Facade (時序資料庫存取)
    * ### Decorator (簡化 Singleton)
    * ### Command (半自動化網管配置)
    * ### Builder (Cisco IOS 命令構建)
* ### 交友軟體
    * ### Singleton (組態管理、資訊管理、資料庫存取、Redis 存取、參數校驗、JWT 管理、配對機制管理、配對狀態、配對池、配對流程管理、Merkle Tree、Email 寄送、聊天管理、好友管理、隨機碼生成器)
    * ### State (配對流程狀態控制)
    * ### DAO (關聯資料庫存取)
    * ### Facade (關聯資料庫存取、交友管理)
    * ### Builder (圖檔處理)
<br />
